module TimeService
  def self.add_minutes(time_string, minutes_to_add)
    time_adder = TimeAdder.new do |config|
      config.time_string = time_string
      config.minutes_to_add = minutes_to_add
    end

    time_adder.time_with_added_minutes
  end

  class Parser
    attr_accessor :time_string

    MERIDIAN_AM = 'AM'
    MERIDIAN_PM = 'PM'

    def initialize
      yield(self)
    end

    def total_minutes
      hours, minutes_and_meridian = @time_string.split(':')
      minutes, meridian = minutes_and_meridian.split(/\s+/)

      military_hours = convert_to_military_hours(hours.to_i, meridian)

      (military_hours * 60) + minutes.to_i
    end

    private

    def convert_to_military_hours(hours, meridian)
      if hours == 12 && meridian == MERIDIAN_AM
        0
      elsif hours == 12 && meridian == MERIDIAN_PM
        12
      elsif meridian == MERIDIAN_PM
        hours + 12
      else
        hours
      end
    end
  end

  # I might not have inherited my classes in this way, but I'm sticking to the instructions.
  class TimeAdder < Parser
    attr_accessor :minutes_to_add

    MIDNIGHT_IN_MINUTES = 60 * 24

    def time_with_added_minutes
      total_minutes_plus = (total_minutes + @minutes_to_add.to_i) % MIDNIGHT_IN_MINUTES

      hours = (total_minutes_plus / 60).to_i
      minutes = total_minutes_plus % 60
      meridian = MERIDIAN_AM

      if hours > 12
        hours = hours - 12
        meridian = MERIDIAN_PM
      elsif hours == 12
        meridian = MERIDIAN_PM
      elsif hours == 0
        hours = 12
      end

      create_time_string(hours, minutes, meridian)
    end

    private

    def create_time_string(hours, minutes, meridian)
      "#{hours}:#{minutes.to_s.rjust(2, '0')} #{meridian}"
    end
  end
end