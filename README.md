# Coding Exercise for Keet by Justin Tanner

The following is my solution for the coding exercise described in `code_challenge.txt`.

## Command

```
ruby run.rb
```

## Tests

I wrote a couple of tests to get things going, to run the tests execute:

```
ruby test.rb
```
