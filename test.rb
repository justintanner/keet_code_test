require 'minitest'
require 'minitest/autorun'
require_relative 'time_service'

class KeetTest < MiniTest::Test
  def test_add_minutes
    time = TimeService.add_minutes('9:13 AM', 10)

    assert_equal time, '9:23 AM'
  end

  def test_time_adder_can_init
    time_adder = TimeService::TimeAdder.new do |config|
      config.time_string = '9:23 AM'
      config.minutes_to_add = 10
    end

    assert_equal time_adder.time_string, '9:23 AM'

  end

  def test_parser_can_init
    parser = TimeService::Parser.new do |config|
      config.time_string = '9:23 AM'
    end

    assert_equal parser.time_string, '9:23 AM'
  end

  def test_total_minutes
    parser = TimeService::Parser.new do |config|
      config.time_string = '9:23 AM'
    end

    assert_equal parser.total_minutes, 563
  end

  def test_total_minutes_midnight
    parser = TimeService::Parser.new do |config|
      config.time_string = '12:00 AM'
    end

    assert_equal parser.total_minutes, 0
  end

  def test_total_minutes_noon
    parser = TimeService::Parser.new do |config|
      config.time_string = '12:00 PM'
    end

    assert_equal parser.total_minutes, 720
  end

  def test_total_minutes_101_am
    parser = TimeService::Parser.new do |config|
      config.time_string = '1:01 AM'
    end

    assert_equal parser.total_minutes, 61
  end

  def test_time_with_added_minutes
    time_adder = TimeService::TimeAdder.new do |config|
      config.time_string = '2:01 AM'
      config.minutes_to_add = 10
    end

    assert_equal time_adder.time_with_added_minutes,'2:11 AM'
  end

  def test_time_with_added_minutes_midnight
    time_adder = TimeService::TimeAdder.new do |config|
      config.time_string = '12:00 AM'
      config.minutes_to_add = 1
    end

    assert_equal time_adder.time_with_added_minutes,'12:01 AM'
  end

  def test_time_with_added_minutes_midday
    time_adder = TimeService::TimeAdder.new do |config|
      config.time_string = '11:59 AM'
      config.minutes_to_add = 1
    end

    assert_equal time_adder.time_with_added_minutes,'12:00 PM'
  end
end
